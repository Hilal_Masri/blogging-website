const sqlite = require("sqlite");
const sqlite3 = require("sqlite3");
const SQL = require("sql-template-strings");

const createtables = async () => {
    const db = await sqlite.open({
        filename: 'db.sqlite',
        driver: sqlite3.Database
    })  

  /**
 * Create the table
 **/
    await db.run(`CREATE TABLE admin (id INTEGER PRIMARY KEY
    AUTOINCREMENT, email VARCHAR(55) NOT NULL, password VARCHAR(25) NOT NULL);`);

    await db.run(`CREATE TABLE contact (id INTEGER PRIMARY KEY
    AUTOINCREMENT, name VARCHAR(55) NOT NULL, email VARCHAR(55) NOT NULL,
    title VARCHAR(55) NOT NULL, message text NOT NULL);`);  
    
    await db.run(`CREATE TABLE subscriber (id INTEGER PRIMARY KEY
    AUTOINCREMENT, email VARCHAR(55) NOT NULL);`);    

    await db.run(`CREATE TABLE blog (id INTEGER PRIMARY KEY
    AUTOINCREMENT, title VARCHAR(55) NOT NULL, description text NOT NULL,
    content TEXT NOT NULL, image BLOB NOT NULL,
    views INTEGER NOT NULL, date DATETIME NOT NULL);`);     

    await db.run(`CREATE TABLE comments (id INTEGER PRIMARY KEY
    AUTOINCREMENT, desc TEXT NOT NULL, date DATETIME NOT NULL,
    idblog INTEGER NOT NULL,FOREIGN KEY (idblog) REFERENCES blog(id));`);      
             
    await db.run(`CREATE TABLE IP (id INTEGER PRIMARY KEY
    AUTOINCREMENT, IP INTEGER NOT NULL, idblog text NOT NULL,
    FOREIGN KEY (idblog) REFERENCES blog(id));`);        
    console.log('tables created!')
}

//delete the tables
const deleteTable = async () => {
    const db = await sqlite.open({
        filename: 'db.sqlite',
        driver: sqlite3.Database
    })
    const tableName=[`admin`,`contact`,`subscriber`,`blog`,`comments`,`IP`];
    let statement=`DROP TABLE `
    tableName.forEach(async (t)=>{
       let sta=statement+t;
        await db.run(sta);

    })
    console.log('tables deleted!');
    
}

module.exports={createtables,deleteTable}